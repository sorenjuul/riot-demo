<callforward>
  <div if={!data}>Loading ...</div>
  <div if={data}>
    Her kan du slå viderestilling af din bredbåndstelefoni til- og fra, og du kan vælge hvilke typer viderestilling du ønsker at aktivere.
    <label class="checkbox callforward">
      <input type="checkbox" onchange={onCheckbox} checked="{data.forwardCalls}">
        Ja tak, slå viderestilling til.
    </label>
    <div if={data.forwardCalls}>
      <div class="form-group" >
        <label >Opkald der skal viderestilles:</label>
        <select class="form-control" id="selectWhichCallsToForward" onchange={changeWhichCallsToForward} >
          <option value="none" >Vælg her</option>
          <option value="all" >Alle opkald</option>
          <option value="noResponsOrBusy" >Opkald hvor der ikke svares eller er optaget</option>
        </select>
      </div>
      <div class="form-group" if={data.whichCallsToForward != 'none'}>
        <label >Opkald viderestilles til:</label>
        <select class="form-control" id="selectForwardCallsTo" onchange={changeForwardCallsTo}>
          <option value="none" >Vælg her</option>
          <option value="voiceMail" >Voice mail</option>
          <option value="phoneNumber" >Andet nummer</option>
        </select>
      </div>
      <div class="form-group {has-error : !numberValidate()}" if={data.whichCallsToForward != 'none' && data.forwardCallsTo == 'phoneNumber'}>
        <label >Nummer der viderestille til:</label>
        <input class="form-control" type="text" value="{data.phoneNumber}" onkeyup={numberChange}>
      </div>
      <div class="form-group" if={ numberValidate() && data.whichCallsToForward != 'none' && data.forwardCallsTo != 'none'}>
        <button type="button" class="btn btn-sm btn-primary" onclick={save} >Gem indstillinger</button>
      </div>
      <div show="{alert}" class="alert alert-success" role="alert">Din viderestilling er blevet ændret.</div>
    </div>
  </div>

  <script>

  var self = this;
  this.data = false;
  this.alert = false;

  this.on('mount', function() {
    setTimeout(function(){
      self.data = {
        forwardCalls:false,
        whichCallsToForward:'all',
        forwardCallsTo:'phoneNumber',
        phoneNumber:'55667787'
      };
      self.selectWhichCallsToForward.value = self.data.whichCallsToForward;
      self.selectForwardCallsTo.value = self.data.forwardCallsTo;
      self.update();
    }, 2000);
  });

  onCheckbox(e){
    this.data.forwardCalls = e.currentTarget.checked;
  }

  changeWhichCallsToForward(e){
    this.data.whichCallsToForward = e.target.value;
  }

  changeForwardCallsTo(e){
    this.data.forwardCallsTo = e.target.value;
  }

  numberChange(e){
    this.data.phoneNumber = e.target.value;
  }

  numberValidate(){
    return this.data && this.data.phoneNumber.length === 8;
  }

  save(){
    console.log(this.data);
    this.alert = true;
    setTimeout(function(){self.alert = false; self.update();}, 2000);
  }

  </script>

</callforward>
