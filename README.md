# README #

### What is this repository for? ###

* Prototyping with Roit.js [https://muut.com/riotjs/](https://muut.com/riotjs/)

### How do I get set up? ###


```
#!shell
$ npm install -g bower gulp
$ npm install
$ bower install
$ gulp serve

```